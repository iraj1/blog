var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  global.db.selectMongo((e, docs) => {
    if(e) { return console.log(e); }
    res.render('index', { title: 'Lista de Posts', docs: docs });
  })
});

router.get('/new', function(req, res, next) {
  res.render('new', { title: 'Novo Post' });
});

router.post('/new', function(req, res) {
  var Titulo = req.body.Titulo;
  var Corpo = req.body.Corpo;
  global.db.insert({Titulo, Corpo}, (err, result) => {
          if(err) { return console.log(err); }
          res.redirect('/');
      })
});
	
router.get('/edit/:id', function(req, res, next) {
  var id = req.params.id;
  global.db.findOne(id, (e, docs) => {
      if(e) { return console.log(e); }
      res.render('new', { title: 'Edição de Post', doc: docs[0], action: '/edit/' + docs[0]._id });
    });
});

router.get('/new', function(req, res, next) {
  res.render('new', { title: 'Novo Post', doc: {"Titulo":"","Corpo":""}, action: '/new' });
});

router.post('/edit/:id', function(req, res) {
  var id = req.params.id;
  var Titulo = req.body.Titulo;
  var Corpo = req.body.Corpo;
  global.db.update(id, {Titulo, Corpo}, (e, result) => {
        if(e) { return console.log(e); }
        res.redirect('/');
    });
});
module.exports = router;
