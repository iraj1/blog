var mongoClient = require("mongodb").MongoClient;
mongoClient.connect("mongodb://localhost/blog")
            .then(conn => global.conn = conn.db("blog"))
            .catch(err => console.log(err))
                
function selectMongo(callback){  
    global.conn.collection("customers").find({}).toArray(callback);
}
 
function insert(customer, callback){
    global.conn.collection("customers").insert(customer, callback);
}
	
var ObjectId = require("mongodb").ObjectId;
function findOne(id, callback){  
    global.conn.collection("customers").find(new ObjectId(id)).toArray(callback);
}
	
function update(id, customer, callback){
    global.conn.collection("customers").updateOne({_id:new ObjectId(id)}, customer, callback);
}
module.exports = { selectMongo, insert, findOne, update }